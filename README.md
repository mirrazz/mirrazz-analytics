# Mirrazz Analytics
The source of Mirrazz's open-source (GNU GPL 3.0) privacy-preserving analytics.

## How to use
You will need to write your own analytics handler.
This script will send all analytics to the handler.

We do not expose our analytics handler for privacy and security reasons (all data stored within Mirrazz is heavily encrypted to provide a large amount of security).

## What this collects
This only collects the following information:

* **If you're on a mobile device.** To see how many mobile device users visit your site.
* **The site origin.** To correctly match analytic data with a certain site.
* **The path of the page.** For big websites, can see which pages are the most popular.

We never collect or store:

* **IP Addresses** We are not IP loggers. We also do not forward your IP to the Handler.
* **Full-Length User Agents** We only detect if the string `" Mobile "` is in the user-agent.
* **Personal Data** We do not collect or forward any personal or sensitive info.

We respect Do-Not-Track requests.
