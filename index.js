const AnalyticsHandler=require('./AnalyticsHandler');
const http=require('http');
http.createServer(function (req,res) {
    if((new URL('https://localhost'+req.url)).pathname==="/analytics/collect") {
        try {
            var origin=new URL(req.headers["origin"]);
            if(origin.protocol!='https:'&&origin.protocol!='http:') {
                throw new Error('d.InvalidOrigin')
            }
            if(!await AnalyticsHandler.domainRegistered(origin.hostname)) {
                throw new Error('d.InvalidHost')
            }
            if(req.headers['DNT']==='1'){
                throw new Error('d.DoNotTrack');
            }
            res.writeHead(200,{'content-type':'text/plain'});
            res.write('200 Analytics Sent');
            res.end();
            AnalyticsHandler.collect({
                origin: origin.hostname,
                mobile: headers['user-agent'].toLowerCase().includes("mobile"),
                path: headers['x-analytics-path'],
            });
        } catch (error) {
            res.writeHead(400,{'content-type':'text/plain'});
            res.write('400 Invalid Request')
            res.end()
        }
    } else {
        res.writeHead(404,{'content-type':'text/plain'});
        res.write('404 Invalid Endpoint');
        res.end();
    }
}).listen(443);
